# METScanning

This project aims to centralize the MET scanners analysis in a more efficient framework.
For more details see [here](https://docs.google.com/document/d/19xXYj_tSPuO78WXTxKVG-ikGWeN6EqSnGlyh7VSafPA/edit).


# Framework Mannual

1. Clone the repository into your local directoy
```
git clone ssh://git@gitlab.cern.ch:7999/mmahdavi/metscanning.git
```
<br/>

2. Prepare the framework before using

   
   Currently the framework is not able to choose the proper `jet Id` according to the `dataset` year.
   So, one should do it mannually at the moment (It will be resolved in the future).

   To do this please:
   - Find the `Jet Id` parameters values:

        [Here](https://gitlab.cern.ch/mmahdavi/metscanning/-/blob/master/config/jet_id.yaml#L3-5) one can see the `Jet Id` `TWiki` pages for 2016, 2017 and 2018 resepectively,
        One should choose the proper one and dig out the information.

   - Update the `NFRegion` Class:

        Now, one should update [these lines](https://gitlab.cern.ch/mmahdavi/metscanning/-/blob/master/NFRegion.cc#L81-102) of `NFRegion` Class, according to the above found information.

<br/>    

3. Setup the environment
```
cd metscanning

. ./env
```
*Please note that one needs to setup the environment each time*
<br/>
<br/>

4. Build the executable file
```
Build
```
<br/>

5. Run the analysis
```
runAnalysis --output output.root --dataset DATASET --year YYYY --pass-rec [ON/off] --cross-check [ON/off] --inputs file1 file2 ... fileN
```
***`--cross-check` option will allow you to have cross-check histograms in NFR***

- One can also run ```runAnalysis --help``` or ```runAnalysis -h``` for more detailed help message.
<br/>

# Plotting Mannual
There are two different python scripts which provide `efficiencies` and `occupancy maps` plots such as [plot_efficencies](https://gitlab.cern.ch/mmahdavi/metscanning/-/blob/master/plot_efficiencies) and [plot_occupancy_maps](https://gitlab.cern.ch/mmahdavi/metscanning/-/blob/master/plot_occupancy_maps) respectively. They are mentioned below separately.

Before using the scripts, please make sure that the environment has been set up as mention above.

- **Occupancy maps:**
```
plot_occupancy_maps [list of output root-files separated by space] --dataset [list of datasets w.r.t provided output root-files separated by space] --year YYYY
```
<br/>

- **Efficiency plots:**

For full plots, i.e all filters plots individually and merged plots for each provided dataset, `--all` option is needed like below.
```
plot_efficencies [list of output root-files separated by space] --dataset [list of datasets w.r.t provided output root-files separated by space] --year 2018 --all
```

If one needs also to obtain the actual final merged plot, all datasets must be provided. In this case the `--all` option is optional, for example, below command will plot all kind of plots and the actual final merged plot as well.
```
plot_efficencies jetht_output.root singlemuon_output.root egamma_output.root --dataset jetht singlemuon egamma --year 2018 --all
```
<br/>

- **Plots Configurations:**

The `plot_efficencies` script uses `yaml` configuration files which contain `binning` settings according to regions (`BER` and `NFR`) variables and/or datasets,  for each filter separately.

The configuration files are provided in the [config](https://gitlab.cern.ch/mmahdavi/metscanning/-/tree/master/config) directory. One may need to modify theses configuration files (as this is the goal of a configuration file indeed) to obtian the proper plots.
<br/>
<br/>

- **One can also run** ```plot_efficencies [--help, -h]``` or ```plot_occupancy_maps [--help, -h]``` for more detailed help messages.