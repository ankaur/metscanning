#ifndef NFREGION_CC
#define NFREGION_CC

#include <TLorentzVector.h>
#include <TVector2.h>

#include "NFRegion.h"

enum {pt, eta, phi, mass, ptr};

NFRegion::NFRegion(TTreeReader &reader, TString dataset, bool fillMore)
    : Tree(reader),
      dataset_{dataset},
      fillMore_{fillMore} {


  if (dataset_ == "jetht") {
    isNFR = &NFRegion::isJetHtNFR;
    if (fillMore_) {
      ptDist_.emplace_back("jet0_pt", "jet0;pt;events", 1000, 0., 1000.);
      ptDist_.emplace_back("jet1_pt", "jet1;pt;events", 1000, 0., 1000.);
      etaDist_.emplace_back("jet0_eta", "jet0;eta;events", 100, -5, 5);
      etaDist_.emplace_back("jet1_eta", "jet1;eta;events", 100, -5, 5);
      phiDist_.emplace_back("jet0_phi", "jet0;phi;events", 100, -3.5, 3.5);
      phiDist_.emplace_back("jet1_phi", "jet1;phi;events", 100, -3.5, 3.5);
      ptRatioDist_ = TH1F("pt_ratio", "pt0/pt1;ratio;events", 100, 0.9, 1.3);
      massDist_ = TH1F("diJet_mass", "dijet mass;mass;events", 1000, 0, 5000);
    }
  }

  else if (dataset_ == "doublemuon") {
    isNFR = &NFRegion::isDMuonNFR;
    if (fillMore_) {
      ptDist_.emplace_back("muon0_pt", "muon0;pt;events", 1000, 0., 1000.);
      ptDist_.emplace_back("muon1_pt", "muon1;pt;events", 1000, 0., 1000.);
      etaDist_.emplace_back("muon0_eta", "muon0;eta;events", 100, -5, 5);
      etaDist_.emplace_back("muon1_eta", "muon1;eta;events", 100, -5, 5);
      phiDist_.emplace_back("muon0_phi", "muon0;phi;events", 90, -3.5, 3.5);
      phiDist_.emplace_back("muon1_phi", "muon1;phi;events", 90, -3.5, 3.5);
      massDist_ = TH1F("Z_mass", "", 1000, 0, 1000);
    }
  }

  else if (dataset_ == "egamma" or dataset_ == "singleelectron") {
    isNFR = &NFRegion::isEGammaNFR;
    if (fillMore_) {
      ptDist_.emplace_back("ele0_pt", "ele0;pt;events", 1000, 0., 1000.);
      ptDist_.emplace_back("ele1_pt", "ele1;pt;events", 1000, 0., 1000.);
      etaDist_.emplace_back("ele0_eta", "ele0;eta;events", 100, -5, 5);
      etaDist_.emplace_back("ele1_eta", "ele1;eta;events", 100, -5, 5);
      phiDist_.emplace_back("ele0_phi", "ele0;phi;events", 90, -3.5, 3.5);
      phiDist_.emplace_back("ele1_phi", "ele1;phi;events", 90, -3.5, 3.5);
      massDist_ = TH1F("Z_mass", "", 1000, 0, 1000);
    }
  }

  else if (dataset_ == "singlemuon") {
    isNFR = &NFRegion::isSMuonNFR;
    if (fillMore_) {
      ptDist_.emplace_back("muon0_pt", "muon0;pt;events", 1000, 0., 1000.);
      etaDist_.emplace_back("muon0_eta", "muon0;eta;events", 100, -5, 5);
      phiDist_.emplace_back("muon0_phi", "muon0;phi;events", 90, -3.5, 3.5);
      phiDist_.emplace_back("pfmet_phi", "PF MET;phi;events", 90, -3.5, 3.5);
      massDist_ = TH1F("mt", "mt of muon+pfmet;m_t;events", 1000, 0, 1000);
    }
  }

  else if (dataset_ == "mc") {
    isNFR = &NFRegion::isSimNFR;
  }
  
}


bool NFRegion::operator()() {
  favoredCandIdx_.clear();
  return (this->*isNFR)();
}


bool NFRegion::passJetId(int i) {
  if (std::fabs((*jet_eta)[i]) <= 2.6) {
    return ((*jet_nhef)[i] < 0.9 and (*jet_neef)[i] < 0.9 and
        ((*jet_chm)[i] + (*jet_nm)[i]) > 1 and (*jet_muef)[i] < 0.8 and
        (*jet_chef)[i] > 0 and (*jet_chm)[i] > 0 and (*jet_ceef)[i] < 0.8);
  }

  else if (std::fabs((*jet_eta)[i]) <= 2.7) {
    return ((*jet_nhef)[i] < 0.9 and (*jet_neef)[i] < 0.99 and
        (*jet_muef)[i] < 0.8 and (*jet_chm)[i] > 0 and (*jet_ceef)[i] < 0.8);
  }

  else if (std::fabs((*jet_eta)[i]) <= 3.0) {
    return ((*jet_neef)[i] > 0.02 and (*jet_neef)[i] < 0.99 and (*jet_nm)[i] > 2);
  }

  else if (std::fabs((*jet_eta)[i]) <= 5.0) {
    return ((*jet_nhef)[i] > 0.2 and (*jet_neef)[i] < 0.9 and (*jet_nm)[i] > 10);
  }

  return false;
}


bool NFRegion::isJetHtNFR() {
  if (*pfmet > 100)
    return false;

  for (int i = 0; i < (*jet_pt).size(); i++) {
    if ((*jet_pt)[i] < 200)
      continue;

    if (NFRegion::passJetId(i))
      favoredCandIdx_.push_back(i);

    if (favoredCandIdx_.size() == 2)
      break;
  }

  if (favoredCandIdx_.size() >= 2) {
    size_t const &zero = favoredCandIdx_[0], &one = favoredCandIdx_[1];
    auto const deltaPhi = std::fabs(
        TVector2::Phi_mpi_pi((*jet_phi)[zero] - (*jet_phi)[one]));
    if (deltaPhi >= 2.9) {
      auto const ptRatio = std::fabs((*jet_pt)[zero] / (*jet_pt)[one]);
      if (ptRatio > 0.8 and ptRatio < 1.2) {
        if (not fillMore_)
          return true;

        TLorentzVector jet0P4(0, 0 ,0 ,0), jet1P4(0, 0, 0, 0);
        jet0P4.SetPtEtaPhiE(
            (*jet_pt)[zero], (*jet_eta)[zero], (*jet_phi)[zero], (*jet_en)[zero]);
        jet1P4.SetPtEtaPhiE(
            (*jet_pt)[one], (*jet_eta)[one], (*jet_phi)[one], (*jet_en)[one]);

        double const diJetMass = (jet0P4 + jet1P4).M();
        Fill(jet_pt, pt);
        Fill(jet_eta, eta);
        Fill(jet_phi, phi);
        Fill(diJetMass, mass);
        Fill(ptRatio, ptr);
        return true;
      }
    }
  }

  return false;
}


bool NFRegion::isDMuonNFR() {
  if (*pfmet > 50)
    return false;

  for (int i = 0; i < (*muon_isPF).size(); i++)
    if ((*muon_isPF)[i] and (*muon_isMedium)[i] == 1 and (*muon_iso)[i] < 0.2 and
        (*muon_pt)[i] > 30)
      favoredCandIdx_.push_back(i);

  if (favoredCandIdx_.size() == 2) {
    size_t const &zero = favoredCandIdx_[0], &one = favoredCandIdx_[1];
    if ((*muon_charge)[zero] * (*muon_charge)[one] != -1)
      return false;

    TLorentzVector mu0P4(0, 0 ,0 ,0), mu1P4(0, 0, 0, 0);

    mu0P4.SetPtEtaPhiE((*muon_pt)[zero], (*muon_eta)[zero],
        (*muon_phi)[zero], (*muon_en)[zero]);
    mu1P4.SetPtEtaPhiE((*muon_pt)[one], (*muon_eta)[one],
        (*muon_phi)[one], (*muon_en)[one]);

    double const invMass = (mu0P4 + mu1P4).M();
    if (invMass > 81 and invMass < 101) {
      if (not fillMore_)
        return true;

      Fill(muon_pt, pt);
      Fill(muon_eta, eta);
      Fill(muon_phi, phi);
      Fill(invMass, mass);
      return true;
    }
  }

  return false;
}


bool NFRegion::isEGammaNFR() {
  if (*pfmet > 50)
    return false;

  for (int i = 0; i < (*ele_pt).size(); i++)
    if ((*ele_pt)[i] > 30.0 and (*ele_isMedium)[i] == 1)
      favoredCandIdx_.push_back(i);

  if (favoredCandIdx_.size() == 2) {
    size_t const &zero = favoredCandIdx_[0], &one = favoredCandIdx_[1];
    if ((*ele_charge)[zero] * (*ele_charge)[one] != -1)
      return false;

    TLorentzVector el0P4(0, 0 ,0 ,0), el1P4(0, 0, 0, 0);

    el0P4.SetPtEtaPhiE((*ele_pt)[zero], (*ele_eta)[zero],
        (*ele_phi)[zero], (*ele_en)[zero]);
    el1P4.SetPtEtaPhiE((*ele_pt)[one], (*ele_eta)[one],
        (*ele_phi)[one], (*ele_en)[one]);

    double const invMass = (el0P4 + el1P4).M();
    if (invMass > 81 and invMass < 101) {
      if (not fillMore_)
        return true;

      Fill(ele_pt, pt);
      Fill(ele_eta, eta);
      Fill(ele_phi, phi);
      Fill(invMass, mass);
      return true;
    }
  }

  return false;
}


bool NFRegion::isSMuonNFR() {
  if (*pfmet > 100.0)
    return false;

  TLorentzVector pfmetP4(0, 0 ,0 ,0);
  pfmetP4.SetPtEtaPhiE(*pfmet, 0, *pfmet_phi, *pfmet);
  for (int i = 0; i < (*muon_isPF).size(); i++)
    if ((*muon_isPF)[i] and (*muon_isMedium)[i] == 1 and (*muon_iso)[i] < 0.2 and
        (*muon_pt)[i] > 30.0) {
      TLorentzVector muP4(0, 0 ,0 ,0);

      muP4.SetPtEtaPhiE(
          (*muon_pt)[i], (*muon_eta)[i], (*muon_phi)[i], (*muon_en)[i]);

      auto const mT = (muP4 + pfmetP4).Mt();
      if (mT <= 120.0) {
        favoredCandIdx_.push_back(i);
        if (not fillMore_)
          return true;

        Fill(muon_pt, pt);
        Fill(muon_eta, eta);
        Fill(muon_phi, phi);
        Fill(*pfmet_phi, phi);
        Fill(mT, mass);
        return true;
      }
    }

  return false;
}


bool NFRegion::isSimNFR() {
  if (*pfmet < 100)
    return true;

  return false;
}


void NFRegion::Fill(doubleVectorReaderValue &var, int const &varType) {
  size_t i = 0;
  switch (varType) {
    case pt:
      for (auto const &idx : favoredCandIdx_)
        ptDist_[i++].Fill((*var)[idx]);
      break;

    case eta:
      for (auto const &idx : favoredCandIdx_)
        etaDist_[i++].Fill((*var)[idx]);
      break;

    case phi:
      for (auto const &idx : favoredCandIdx_)
        phiDist_[i++].Fill((*var)[idx]);
      break;
  }
}


void NFRegion::Fill(double const &var, int const &varType) {
  switch (varType) {
    case mass:
      massDist_.Fill(var);
      break;

    case ptr:
      ptRatioDist_.Fill(var);
      break;
  }
}

#endif
