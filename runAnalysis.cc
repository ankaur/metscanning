#include <bitset>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <forward_list>
#include <iostream>
#include <math.h>

#include <TVector2.h>
#include <TChain.h>

#include "MetFilter.h"
#include "NFRegion.h"
#include "Tree.h"


using namespace boost::program_options;

struct Filter {
  TString name;
  TString flag;
  int passRecKey;
  Filter(TString name, TString flag, int passRecKey=-1)
    : name{name}, flag{flag}, passRecKey{passRecKey} {}
};


int main(int argc, char *argv[]) {

  unsigned int step;
  std::string output, dataset, year;
  bool isVerbose, crossCheckPlots, passRecOption;
  std::vector<std::string> inputs;

  try {
    options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("year", value<std::string>(&year)->required(), "Dataset year")
      ("output,o", value<std::string>(&output)->required(), "Output root file")
      ("dataset", value<std::string>(&dataset)->required(), "Dataset name")
      ("verbose,v", value<bool>(&isVerbose)->default_value(false), "Show progress")
      ("pass-rec", value<bool>(&passRecOption)->default_value(true),
       "Require events to pass recommended MET filters except the one under study")
      ("inputs", value<std::vector<std::string>>(&inputs)->
       multitoken()->zero_tokens()->composing(), "List of input files")
      ("step", value<unsigned int>(&step)->default_value(100000), "Progress steps")
      ("cross-check", value<bool>(&crossCheckPlots)->default_value(false),
       "Plot cross-check histograms");

    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);

    if (vm.count("help")) {
      std::cout << desc << '\n';
      return false;
    }
    notify(vm);
  }
  catch (const error &ex) {
    std::cerr << ex.what() << '\n';
    return false;
  }

  /** Defining Variables **/
  TString datasetYear = dataset + "_" + year;
  boost::algorithm::to_lower(dataset);
  // Stores which recommended MET filters are passed.
  int passRecStatus = 255;

  TFile outputRootFile(output.c_str(), "RECREATE");

  // Variables used for MET filters indices.
  enum FiltersName{GoodVertices, GlbSTightHalo, HBHENoise, HBHEIsoNoise, EcalTP,
    EEBadSc, EcalBadCal_U, BadPFMuon, BadPFMuon_U, BadPFMuon_DzU, EcalDCBE,
    EcalDCBE_U, GlbTightHalo, BadChCand, BadChCand_U};

  enum EffCat{nfr_pfmet, nfr_pupmet, nfr_lJpt, nfr_npvx,
              ber_pfmet, ber_pupmet, ber_lJpt, ber_npvx};

  enum OccMapCat{nfr, ber};

  std::map<unsigned short int, std::array<TString, 2>> effCat;
  std::map<unsigned short int, TString> occMap;

  unsigned short int I = 0, J = 0;
  for (auto const &region : MetFilter::regions) {
    occMap[I++] = region.first;
    for (auto const &var : MetFilter::vars) {
      effCat[J++] = {region.first, var};
    }
  }

  TChain chain("ntuplemakerminiaod/tree");
  for (auto const &file : inputs)
    chain.Add(file.c_str());

  TTreeReader reader(&chain);
  Tree tree(reader);
  NFRegion isNFR(reader, dataset, crossCheckPlots);
  auto npvx =  tree.npvx;
  auto run =  tree.run;
  auto lumi =  tree.lumi;
  auto event =  tree.event;
  auto pfmet =  tree.pfmet;
  auto pfmet_phi =  tree.pfmet_phi;
  auto puppimet =  tree.puppimet;

  auto jet_pt =  tree.jet_pt;
  auto jet_eta =  tree.jet_eta;
  auto jet_phi =  tree.jet_phi;
  auto jet_chef =  tree.jet_chef;
  auto jet_neef =  tree.jet_neef;
  auto jet_nhef =  tree.jet_nhef;

  auto muon_pt =  tree.muon_pt;
  auto muon_eta =  tree.muon_eta;
  auto muon_phi =  tree.muon_phi;

  auto pfCand_pt =  tree.pfCand_pt;
  auto pfCand_phi =  tree.pfCand_phi;
  auto pfCand_pdgid =  tree.pfCand_pdgid;
  auto num_jets_pt25 = tree.num_jets_pt25;

  std::array<int, 8> recMetFilters = {0, 1, 2, 3, 4, 5, 6, 7};
  std::array<Filter, 15> filtersList {{
    {"GoodVertices", "Flag_goodVertices", 254},
    {"GlbSTightHalo", "Flag_globalSuperTightHalo2016Filter", 253},
    {"HBHENoise", "Flag_HBHENoiseFilter", 251},
    {"HBHEIsoNoise", "Flag_HBHENoiseIsoFilter", 247},
    {"EcalTP", "Flag_EcalDeadCellTriggerPrimitiveFilter", 239},
    {"EEBadSc", "Flag_eeBadScFilter", 223},
    {"EcalBadCal_U", "PassecalBadCalibFilter_Update", 191},
    {"BadPFMuon", "Flag_BadPFMuonFilter", 127},
    {"BadPFMuon_U", "PassBadPFMuonFilter_Update", 127},
    {"BadPFMuon_Dz_U", "PassBadPFMuonFilterDz_Update", 127},
    {"EcalDCBE", "Flag_EcalDeadCellBoundaryEnergyFilter"},
    {"EcalDCBE_U", "PassEcalDeadCellBoundaryEnergyFilter_Update"},
    {"GlbTightHalo", "Flag_globalTightHalo2016Filter", 253},
    {"BadChCand", "Flag_BadChargedCandidateFilter"},
    {"BadChCand_U", "PassBadChargedCandidateFilter_Update"}}}; 

  /* Some Explanation about the numbers like 254, 253, ... in above:
     For example for second filter i.e. GlbSTightHalo the passRecKey is 253
     which in binary is 11111101, which means all recommended filters are passed
     except for second filter itself, which is 0.
     So  in order to fill the histograms for such filter,
     if you require the "event filters status" to be (11111101 OR 11111111),
     then indeed, we have simultaneously applied N-1 efficiency for recommeded
     MET filters and requiring non-recommended met filters to pass all
     recommended filters. */

  int filtersNum = filtersList.size();

  std::vector<MetFilter> metFilters;
  for (auto const &f : filtersList) {
   metFilters.emplace_back(f.name, f.flag, datasetYear, reader, f.passRecKey);
  }

  for (auto &f : metFilters) {
   f.SetDirectory(outputRootFile);
  }

  /** End of Defining Variables **/


  // Logging Setting
  unsigned int event_i = 1;
  int statistics[7] = {0, 0, 0, 0, 0, 0, 0};

  while(reader.Next()) {
    if (isVerbose)
      if (event_i % step == 0) 
        std::cout << event_i << " events are processed" << std::endl;

    // Applying N-1 performance analysis if the option is enabled
    passRecStatus = -1;
    if (passRecOption) {
      passRecStatus = 255;
      for (auto const &i : recMetFilters) {
        if (!metFilters[i].IsPassed())
          passRecStatus ^= 1 << i;
      }
    }

    /* GOING TO NOISE FREE RIGIONS BASED ON THE DATASET */
    if (isNFR()) {
      statistics[0]++;
      for (int f = 0; f < filtersNum; f++) {
        metFilters[f].Fill(*pfmet, effCat[nfr_pfmet], passRecStatus);
        metFilters[f].Fill(*puppimet, effCat[nfr_pupmet], passRecStatus);
        metFilters[f].Fill(*npvx, effCat[nfr_npvx], passRecStatus);
        if ((*jet_pt).size() > 0)
          metFilters[f].Fill((*jet_pt)[0], effCat[nfr_lJpt], passRecStatus);
        metFilters[f].Fill(jet_eta, jet_phi, occMap[nfr], passRecStatus);
      }
    }


    /* GOING TO BACKGROUND ENRICHED RIGIONS BASED ON DIFFERENT FILTERS */

    // BER for BadChCand, BadChCand_U filters
    if (*pfmet > 500) {
      int i = 0;
      for (int j = 0; j < (*pfCand_pt).size(); j++) {
        if ((*pfCand_pt)[j] > 500 and std::abs((*pfCand_pdgid)[j]) == 211 and
            std::fabs(TVector2::Phi_mpi_pi(*pfmet_phi - (*pfCand_phi)[j])) >= 3)
          i++;
      }
      if (i >= 1) {
        statistics[1]++;
        for (auto const & f : {BadChCand, BadChCand_U}) {
          metFilters[f].Fill(*pfmet, effCat[ber_pfmet], passRecStatus);
          metFilters[f].Fill(*puppimet, effCat[ber_pupmet], passRecStatus);
          metFilters[f].Fill(*npvx, effCat[ber_npvx], passRecStatus);
          if ((*jet_pt).size() > 0)
            metFilters[f].Fill((*jet_pt)[0], effCat[ber_lJpt], passRecStatus);
          metFilters[f].Fill(jet_eta, jet_phi, occMap[ber], passRecStatus);
        }
      }
    }

    if (*pfmet > 200 and (*jet_pt).size() == 1 and (*num_jets_pt25) == 1) {

    // BER for GlbSTightHalo, GlbTightHalo filters
      if ((std::fabs(TVector2::Phi_mpi_pi(*pfmet_phi)) < 0.2 or
            std::fabs(TVector2::Phi_mpi_pi(*pfmet_phi)) > 2.9) and
          (*jet_pt)[0] > 200 and std::fabs((*jet_eta)[0]) < 2.4 and
          (*jet_chef)[0] < 0.01) {
        statistics[2]++;
        for (auto const & f : {GlbSTightHalo, GlbTightHalo}) {
          metFilters[f].Fill(*pfmet, effCat[ber_pfmet], passRecStatus);
          metFilters[f].Fill(*puppimet, effCat[ber_pupmet], passRecStatus);
          metFilters[f].Fill(*npvx, effCat[ber_npvx], passRecStatus);
          metFilters[f].Fill((*jet_pt)[0], effCat[ber_lJpt], passRecStatus);
          metFilters[f].Fill(jet_eta, jet_phi, occMap[ber], passRecStatus);
        }
      }

    // BER for BadPFMuon, BadPFMuon_U filters
      if ((*muon_pt).size() > 0) {
        if ((*muon_pt)[0] > 200 and
            std::fabs(TVector2::Phi_mpi_pi(*pfmet_phi - (*muon_phi)[0])) >= 3) {
          if (((*muon_pt).size() > 1) ? ((*muon_pt)[1] < 10) : true) {
            int k = 0;
            for (int j = 0; j < (*jet_pt).size(); j++) {
              // To be sure that the jets are not comming from muon jet cone
              if (std::sqrt(
                    std::pow((*muon_eta)[0] - (*jet_eta)[j], 2) +
                    std::pow((*muon_phi)[0] - (*jet_phi)[j], 2)) < 0.4)
                continue;

              k++;
              break;
            }
            if (k == 0) {
              statistics[3]++;
              for (auto const & f : {BadPFMuon, BadPFMuon_U, BadPFMuon_DzU}) {
                metFilters[f].Fill(*pfmet, effCat[ber_pfmet], passRecStatus);
                metFilters[f].Fill(*puppimet, effCat[ber_pupmet], passRecStatus);
                metFilters[f].Fill(*npvx, effCat[ber_npvx], passRecStatus);
                metFilters[f].Fill((*jet_pt)[0], effCat[ber_lJpt], passRecStatus);
                metFilters[f].Fill(jet_eta, jet_phi, occMap[ber], passRecStatus);
              }
            }
          }
        }
      }

    // BER for EcalBadCalib_U filter
      if ((*jet_pt)[0] > 200 and std::fabs((*jet_eta)[0]) > 2.5 and
          std::fabs((*jet_eta)[0]) < 3 and (*jet_neef)[0] > 0.9 and
          std::fabs(TVector2::Phi_mpi_pi(*pfmet_phi - (*jet_phi)[0])) > 3) {
        statistics[4]++;
        for (auto const & f : {EcalBadCal_U}) {
          metFilters[f].Fill(*pfmet, effCat[ber_pfmet], passRecStatus);
          metFilters[f].Fill(*puppimet, effCat[ber_pupmet], passRecStatus);
          metFilters[f].Fill(*npvx, effCat[ber_npvx], passRecStatus);
          metFilters[f].Fill((*jet_pt)[0], effCat[ber_lJpt], passRecStatus);
          metFilters[f].Fill(jet_eta, jet_phi, occMap[ber], passRecStatus);
        }
      }

    // BER for HBHENoise, HBHEIsoNoise filters
      if ((*jet_pt)[0] > 200 and std::fabs((*jet_eta)[0]) < 3 and
          (*jet_nhef)[0] > 0.9 and
          std::fabs(TVector2::Phi_mpi_pi(*pfmet_phi - (*jet_phi)[0])) > 3) {
        statistics[5]++;
        for (auto const & f : {HBHENoise, HBHEIsoNoise}) {
          metFilters[f].Fill(*pfmet, effCat[ber_pfmet], passRecStatus);
          metFilters[f].Fill(*puppimet, effCat[ber_pupmet], passRecStatus);
          metFilters[f].Fill(*npvx, effCat[ber_npvx], passRecStatus);
          metFilters[f].Fill((*jet_pt)[0], effCat[ber_lJpt], passRecStatus);
          metFilters[f].Fill(jet_eta, jet_phi, occMap[ber], passRecStatus);
        }
      }
    }
    
    // BER for the rest of filters
    if (*pfmet > 200) {
      statistics[6]++;
      for (auto const & f :
          {GoodVertices, EcalTP, EEBadSc, EcalDCBE, EcalDCBE_U}) {
        metFilters[f].Fill(*pfmet, effCat[ber_pfmet], passRecStatus);
        metFilters[f].Fill(*puppimet, effCat[ber_pupmet], passRecStatus);
        metFilters[f].Fill(*npvx, effCat[ber_npvx], passRecStatus);
        if ((*jet_pt).size() > 0)
          metFilters[f].Fill((*jet_pt)[0], effCat[ber_lJpt], passRecStatus);
        metFilters[f].Fill(jet_eta, jet_phi, occMap[ber], passRecStatus);
      }
    }

    event_i++;
  }
  std::cout << "Total_events " << event_i << std::endl;
  for (int m = 0; m < sizeof(statistics) / sizeof(statistics[0]); m++)
    std::cout << m << " " << statistics[m] << std::endl;
  outputRootFile.cd();
  outputRootFile.Write();
  outputRootFile.Close();
}

