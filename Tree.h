#ifndef TREE_H
#define TREE_H


typedef TTreeReaderValue<std::vector<double>> doubleVectorReaderValue;

class Tree {
 public:
   // Constructor
   Tree(TTreeReader &reader);

   doubleVectorReaderValue jet_pt, jet_eta, jet_phi, jet_en;
   doubleVectorReaderValue jet_chef, jet_neef, jet_nhef, jet_muef, jet_ceef;
   doubleVectorReaderValue muon_pt, muon_eta, muon_phi, muon_en, muon_iso;
   doubleVectorReaderValue ele_pt, ele_eta, ele_phi, ele_en, pfCand_pt, pfCand_phi;

   TTreeReaderValue<std::vector<int>> pfCand_pdgid, jet_chm, jet_nm,
     muon_isMedium, ele_isMedium, muon_charge, ele_charge;

   TTreeReaderValue<std::vector<bool>> muon_isPF;

   TTreeReaderValue<double> pfmet, pfmet_phi, puppimet;

   TTreeReaderValue<ULong64_t> run, event, lumi;

   TTreeReaderValue<int> npvx, num_jets_pt25;
};

#endif
#include "Tree.cc"
