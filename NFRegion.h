#ifndef NFREGION_H
#define NFREGION_H

#include "Tree.h"

#include <TLorentzVector.h>


class NFRegion final : private Tree {
 public:
   // Constructor
   NFRegion(TTreeReader &reader, TString dataset, bool fillMore);

   bool operator()();

 private:
   // Checks whether a given jet is pass JetID
   bool passJetId(int i);

   // Check whether the event is in Noise Free Region for JetHT dataset
   bool isJetHtNFR();

   // Check whether the event is in Noise Free Region for Double Muon dataset
   bool isDMuonNFR();

   // Check whether the event is in Noise Free Region for
   // EGamma or Single Electron dataset
   bool isEGammaNFR();

   // Check whether the event is in Noise Free Region for Single Muon dataset
   bool isSMuonNFR();

   // Check whether the event is in Noise Free Region for MC datasets
   bool isSimNFR();

   void Fill(doubleVectorReaderValue &var, int const &varType);
   void Fill(double const &var, int const &varType);

   bool (NFRegion::*isNFR)();

   bool fillMore_;

   TString dataset_;

   std::vector<size_t> favoredCandIdx_;

   std::vector<TH1F> ptDist_, etaDist_, phiDist_;
   TH1F massDist_, ptRatioDist_;
};

#endif
#include "NFRegion.cc"
