#ifndef TREE_CC
#define TREE_CC

#include "Tree.h"


Tree::Tree(TTreeReader &reader)
    : npvx(reader, "_n_PV"),
      run(reader, "_runNb"),
      lumi(reader, "_lumiBlock"),
      event(reader, "_eventNb"),
      pfmet(reader, "_met"),
      pfmet_phi(reader, "_met_phi"),
      puppimet(reader, "_puppimet"),
      jet_pt(reader, "_jetPt"),
      jet_eta(reader, "_jetEta"),
      jet_phi(reader, "_jetPhi"),
      jet_en(reader, "_jetEnergy"),
      jet_chef(reader, "_jet_CHEF"),
      jet_neef(reader, "_jet_NEEF"),
      jet_nhef(reader, "_jet_NHEF"),
      jet_muef(reader, "_jet_MUEF"),
      jet_ceef(reader, "_jet_CEEF"),
      jet_chm(reader, "_jet_CHM"),
      jet_nm(reader, "_jet_NM"),
      muon_pt(reader, "_muonPt"),
      muon_eta(reader, "_muonEta"),
      muon_phi(reader, "_muonPhi"),
      muon_en(reader, "_muonEnergy"),
      muon_charge(reader, "_muonCharge"),
      muon_isMedium(reader, "_muonMedium"),
      muon_iso(reader, "_muonIsoSumDR04"),
      muon_isPF(reader, "_muonPF"),
      ele_pt(reader, "_elePt"),
      ele_eta(reader, "_eleEta"),
      ele_phi(reader, "_elePhi"),
      ele_en(reader, "_eleEnergy"),
      ele_charge(reader, "_eleCharge"),
      ele_isMedium(reader, "gsf_VID_cutBasedElectronID_Fall17_94X_V2_medium"),
      pfCand_pt(reader, "_PFcand_pt"),
      pfCand_phi(reader, "_PFcand_phi"),
      pfCand_pdgid(reader, "_PFcand_pdgId"),
      num_jets_pt25(reader, "_number_jets_pt25") {
}

#endif
