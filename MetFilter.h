#ifndef METFILTER_H
#define METFILTER_H

#include <iostream>
#include <map>
#include <math.h>

#include "TEfficiency.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTree.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"


typedef TTreeReaderValue<std::vector<double>> dValVec;


class MetFilter {
 public:
   // COnstructor
   MetFilter(TString const &name, TString const &flag, TString const &dataset,
             TTreeReader &reader, int const &passRecKey);
   // Destructor
   ~MetFilter() noexcept;

   // A method for filling filters efficiencies
   void Fill(double const &x, std::array<TString, 2> const &info,
       int const &passRecStatus);

   // A method for filling filters occupancy maps
   void Fill(dValVec &etaVec, dValVec &phiVec, TString const &info,
       int const &passRecStatus);

   // Setting directory of output root file
   void SetDirectory(TFile &outputRootFile);

   // Checkes whether the events id passed the filter or not.
   bool IsPassed();

   // Retruns the filter name
   TString GetName() const;

   // Retruns the filter flag
   TString GetFlag() const;

   static const std::array<std::pair<TString, TString>, 2> regions;

   static const std::array<TString, 4> vars;

 private:
   TString const name_;

   TString const flag_;

   // Indicates whether other filters are passed recommended filters.
   int passRecKey_;

   TTreeReaderValue<Bool_t> isPassed_;

   std::vector<TEfficiency> efficiency_;

   std::vector<TH1F> numerator_, denominator_;

   std::vector<TH2F> failedOccupancyMap_, passedOccupancyMap_;

   std::map<TString, std::map<TString, unsigned short int>> effIndex_;

   std::map<TString, unsigned short int> occMapIndex_;
};

#endif
#include "MetFilter.cc"
